# BACKEND DE CATALOGO DE SOFTWARE LIBRE

Backend de catalogo de software libre, api de repositorios, usuarios, proyectos

## URL

> https://desarrollo.adsib.gob.bo/validador_doc/

## Requisitos

 - **GIT** 
 - **CURL**
 - **NODEJS**(>= 6.X.X)

## Instalar Proyecto

Para realizar la instalación y configuración del proyecto revise el archivo [INSTALL.md](INSTALL.md)

## Ejecutar Proyecto

Instalar las dependencias del proyecto

> $ npm install

Iniciar el proyecto

> $ npm start
